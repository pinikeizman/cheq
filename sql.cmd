#Create schema
CREATE SCHEMA CHEQ;

#Use CHEQ chema
USE CHEQ;

#Create Vast table
CREATE TABLE IF NOT EXISTS Vasts(
id int UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL , 
vast_url varchar(600) NOT NULL,
position varchar(20) DEFAULT 'bottom_right' CHECK (country IN ("top_left","top_middle","top_right","middle_left","middle_right","bottom_left","bottom_middle","bottom_right")), 
hide_ui tinyint(1) DEFAULT 0
);