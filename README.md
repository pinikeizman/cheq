# CHEQ Full Stack Interview

CHEQ Full stack interview solution including NodeJS backend and React/Redux 
frontend.

#Where to start...

1. Clone project sources from: `https://bitbucket.org/pinikeizman/cheq.git` into <root>.
2. Create mySql schema and table, commands can be found at `<root>/sql.cmd`.
3. Set mySql connection credentials, there are 2 options:
    a. Enter your credentials into `<root>/src/credentials.js` under MY_SQL object.
    b. Override `MY_SQL_USERNAME` and `MY_SQL_PASSWORD` environment variables
       inside  `<root>/package.json` `start:cedentials` script.
4. Hit under `<root>` the command `npm install` (be sure to have node installed).
5. Hit under `<root>` the command:
    If you picked 2.a you can run `npm start`/`yarn start`.
    If you picked 2.b you can run `npm start start:credentials`/`yarn run start:credentials`.
    