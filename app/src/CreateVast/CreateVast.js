import {connect} from 'react-redux'
import React, {Component} from 'react';
import {FlexBox, ErrorLabel, StyledButton, StyledInput, StyledLabel,StyledJsonArea,StyledTextArea} from '../Styled/styled';
import actions from '../actions/index';
import _ from 'lodash';
import styled from 'styled-components';


const createVastAction = (vast) => {

    return {
        type: actions.API,
        payload: {
            url: "/create_vast",
            accept: "application/json",
            method: "POST",
            body: JSON.stringify(vast)
        }
    }

};

const urlRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/
const booleanValues = [0, 1, "0", "1", "true", "false"];
const possiblePositions = ["top_left", "top_middle", "top_right",
    "middle_left", "middle_right", "bottom_left",
    "bottom_middle", "bottom_right"];


const styles = {
    container: {
        marginTop: 15,
        marginBottom: 15,
    }
};

class CreateVast extends Component {
    state = {
        vastURL: "",
        position: "",
        hideUI: "",
        errors: {},
        vast: {}
    };

    handleOnChange = (value, field) => {
        this.setState({
            [field]: value
        }, this.validate);
    };

    validate = () => {
        const errors = {};
        if (!urlRegex.test(this.state.vastURL)) {
            _.set(errors, "vastURL", "Vast url must be a valid url.");
        }
        if (this.state.position != "" &&
            possiblePositions.indexOf(this.state.position) == -1) {
            _.set(errors, "position", "Vast position must be one of: " + possiblePositions);
        }
        if (this.state.hideUI != "" &&
            booleanValues.indexOf(this.state.hideUI) == -1) {
            _.set(errors, "hideUI", "Vast hide UI must be boolean value: 0,1,'0','1','true','false'.");
        }
        this.setState({errors});
    };

    createVastHandler = () => {

        this.props.createVastAction({
            vastURL: !this.state.vastURL ? undefined : this.state.vastURL,
            position: !this.state.position ? undefined : this.state.position,
            hideUI: !this.state.hideUI ? undefined : this.state.hideUI
        }).then(response => {
                if (response.status != 200) {
                    throw response.text();
                }

                return response.json()
            }
        ).then((body) => {
            this.setState((prevState) => ({
                error: undefined,
                vast: {
                    vastURL: prevState.vastURL,
                    position: !prevState.position ? "bottom_right":prevState.position,
                    hideUI: !prevState.hideUI ? "false" : prevState.hideUI,
                    id: body.insertId
                }
            }));
        }).catch((err) => {
            err.then(txt => this.setState({error: txt}));
        });
        ;
    };

    render() {
        return (
            <FlexBox flex="1">
                <FlexBox style={{width: "50%"}} flexDirection="column" alignItems="center">
                    <FlexBox style={styles.container}>
                        <StyledLabel>
                            Vast Url:
                        </StyledLabel>
                        <StyledTextArea
                            onChange={(e) => this.handleOnChange(e.target.value, "vastURL")}
                            style={{height: 120}}
                            value={this.state.vastURL}/>
                    </FlexBox>
                    {this.state.errors.vastURL ?
                        <ErrorLabel style={styles.container}>{this.state.errors.vastURL}</ErrorLabel> : null }
                    <FlexBox style={styles.container}>
                        <StyledLabel>
                            Vast Position:
                        </StyledLabel>
                        <StyledInput
                            onChange={(e) => this.handleOnChange(e.target.value, "position")}
                            value={this.state.position}/>

                    </FlexBox>
                    {this.state.errors.position ?
                        <ErrorLabel style={styles.container}>{this.state.errors.position}</ErrorLabel> : null }
                    <FlexBox >
                        <StyledLabel>
                            Vast Hide UI:
                        </StyledLabel>
                        <StyledInput
                            onChange={(e) => this.handleOnChange(e.target.value, "hideUI")}
                            value={this.state.hideUI}/>
                    </FlexBox>
                    {this.state.errors.hideUI ?
                        <ErrorLabel style={styles.container}>{this.state.errors.hideUI}</ErrorLabel> : null }
                    <StyledButton
                        onClick={this.createVastHandler}
                        style={styles.container}>
                        Add Vast
                    </StyledButton>
                </FlexBox>
                <FlexBox flexDirection="column"
                         style={{width: "50%", border: '2px solid #fe0072', padding: 10, margin: '15px 0px'}}>
                    <StyledLabel style={{borderBottom: '2px dashed #fe0072'}}>
                        Result:
                    </StyledLabel>
                    <FlexBox style={{padding: 10}}>
                        <StyledJsonArea style={{width: 400, height: 400}}
                                  onChange={() => ({})}
                                  value={this.state.error ? this.state.error : JSON.stringify(this.state.vast)}></StyledJsonArea>
                    </FlexBox>
                </FlexBox>
            </FlexBox>
        )
    }
}

const mapStateToProps = (state) => ({
    vasts: state.vasts
});
export default connect(mapStateToProps, {
    createVastAction
})(CreateVast);