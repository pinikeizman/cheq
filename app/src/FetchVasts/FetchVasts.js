import {connect} from 'react-redux'
import React, {Component} from 'react';
import styled from 'styled-components';
import {FlexBox, ErrorLabel, StyledButton, StyledInput, StyledLabel, StyledJsonArea} from '../Styled/styled';
import actions from '../actions/index';

const createFetchApiAction = (vastId) => ({
    type: actions.API,
    payload: {
        url: "/fetch_vast?id=" + vastId,
        accept: "application/xml"
    }
});
const addVast = (vastId, vastXml) => ({
    type: actions.ADD_VAST,
    payload: {
        vastId, vastXml
    }
});
const changeCurrentVast = (vastId) => ({
    type: actions.CHANGE_CURRENT_VAST,
    payload: {
        vastId
    }
});
const createSetCurrentVast = (vastId) => ({
    type: actions.CHANGE_CURRENT_VAST,
    payload: {
        vastId: vastId
    }
});

const BrandFlexBox = styled(FlexBox)`
     * {font-size:14px;}
`;


class FetchVasts extends Component {

    state = {
        inputValue: this.props.vasts.currentVast || "",
        currentVast: this.props.vasts.currentVast,
        error404: undefined
    };

    handleFetchVast = function () {
        const requestedVast = this.props.vasts[this.state.inputValue];
        if (this.state.inputValue && !requestedVast) {
            const res = this.props.createFetchApiAction(this.state.inputValue);
            res.then((response) => {
                if (response.status != 200) {
                    throw response.text();
                }
                return response.text();
            })
                .then((body) => {
                    this.props.addVast(this.state.inputValue, body);
                    this.props.changeCurrentVast(this.state.inputValue);
                    this.setState({
                        currentVast: this.state.inputValue,
                        error404: undefined
                    });
                }).catch(err => {
                err.then((txt) => {
                    this.setState({error404: txt});
                    });
            });
        } else if (this.state.inputValue) {
            this.setState((nextState) => ({currentVast: nextState.inputValue , error404:undefined}));
        }
    }.bind(this);

    handleInputCHange = (e) => {
        this.setState({inputValue: e.target.value});
    };

    render() {
        return (
            <FlexBox style={{flex: 1}} flexDirection="column" justifyContent="center">
                <FlexBox justifyContent="center">
                    <StyledLabel>Vast ID:</StyledLabel>
                    <StyledInput textAlign="center" value={this.state.inputValue} onChange={this.handleInputCHange}
                                 type="text"/>
                    <StyledButton onClick={this.handleFetchVast}>Get Vast!</StyledButton>
                </FlexBox>
                {this.state.error ? <ErrorLabel>error: {this.state.error }  </ErrorLabel> : null}
                <FlexBox justifyContent="center">
                </FlexBox>
                <FlexBox style={{padding: 10}}>
                    <StyledJsonArea style={{width: '100%',fontSize:14, height: 400}}
                                    onChange={() => ({})}
                                    value={this.state.error404 || this.props.vasts[this.state.currentVast]}>
                    </StyledJsonArea>
                </FlexBox>
            </FlexBox>
        );
    }
}
;

const mapStateToProps = state => {

    return {
        vasts: state.vasts
    }

};

export default connect(
    mapStateToProps,
    {createFetchApiAction, createSetCurrentVast, addVast, changeCurrentVast}
)(FetchVasts)
