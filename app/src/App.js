import React, {Component} from 'react';
import './App.css';
import Logo from './logo.jpg';
import TabComponent from './TabComponent/TabComponent';
import FetchVast from './FetchVasts/FetchVasts';
import CreateVast from './CreateVast/CreateVast';

class App extends Component {

    tabs = [
        {
            title: "Fetch Vasts",
            component: <FetchVast/>
        },
        {
            title: "Post Vasts",
            component: <CreateVast/>

        }
    ];

    render() {
        return (
            <div className="App">
                <div className="HeaderContainer">
                    <img className="LogoImg" src={Logo}/>
                </div>
                <div className="ContentContainer">
                    <TabComponent tabs={this.tabs}/>
                </div>
            </div>
        );
    }
}

// const mapStateToProps = (state, props) => ({
//     vasts: state.vasts
// });

export default App;
