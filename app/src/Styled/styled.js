import styled from "styled-components";

export const FlexBox = styled.div`
                display:flex;
                flex: ${props => !props.flex ? "" : props.flex };
                flex-direction: ${props => !props.flexDirection ? "" : props.flexDirection };
                justify-content:${props => !props.justifyContent ? "" : props.justifyContent};
                align-items:${props => !props.alignItems ? "" : props.alignItems}
`;
export const ErrorLabel = styled.span`
    font-size: 14px;
    color:red;
    margin-top:10px;
    margin-bottom:10px;
`;

export const StyledInput = styled.input`
    height:40px;
    color: #333;
    font-size:18px;
    min-width:350px;
    text-align:${ props => !props.textAlign ? "left" : props.textAlign};
    box-sizing: border-box;
    border: 2px solid #fe0072;
    padding-left: 15px;
    :focus{
        outline:none;
        border: 2px solid #fe0072;
        box-shadow: 0px 0px 5px 1px rgba(208, 7, 97, .5);
    }
`;

export const StyledLabel = styled.label`
    color:#fe0072;
    height:40px;
    font-size:18px;
    font-weight: bold;
    background-color:white;
    line-height:40px;
    min-width:150px;
    margin-right:25px;
`;

export const StyledButton = styled.button`
    margin-left:25px;
    color:#fe0072;
    font-size:18px;
    text-align:center;
    border: 2px solid #fe0072;
    height:40px;
    min-width:150px;
    :hover{
        background-color:#fe0072
        color:white;
    }
    :active{
        transform:scale(0.8,0.8);
        outline:none;
    }
     :focus{
        outline:none;
    }
    
`;

export const StyledTextArea = StyledInput.withComponent('textarea');


export const StyledJsonArea = styled(StyledTextArea)`
    width: 100%;
    border: none;
    font-size: 24px;
    :focus{
        box-shadow: none;
        border: none;
    }
`;

export default {
    FlexBox: FlexBox,
    ErrorLabel: ErrorLabel,
    StyledInput: StyledInput,
    StyledLabel: StyledLabel,
    StyledButton: StyledButton,
    StyledTextArea:StyledTextArea,
    StyledJsonArea: StyledJsonArea


}