import actions from '../actions/';
import _ from "lodash";

const initialState = {
    vasts: {}
};

export default function tabs(state = initialState, action) {
    switch (action.type) {
        case actions.ADD_VAST:
            return _.set(state, "vasts." + action.payload.vastId, action.payload.vastXml);
        case  actions.CHANGE_CURRENT_VAST:
            return _.set(state, "vasts.currentVast", action.payload.vastId);
        default:
            return state
    }
}