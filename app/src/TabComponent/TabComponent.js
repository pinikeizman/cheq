import React, {Component} from 'react';
import styled from 'styled-components';
import {FlexBox} from '../Styled/styled';

const TabHeader = (props) => {
    const TabHeaderComponent = styled(FlexBox)`
        transition: background-color 350ms ease;
        flex:1;
        color:white;
        font-size:28px;
        line-height:32px;
        align-items:center;
        justify-content:center;
        height:40px;
        background-color:#fe0072;
        border-right: solid 20px white;
        border-left: solid 20px white;
        :hover{
            background-color:#d00761;
        }
        &.active{
            background-color:#d00761;
        }
    `;
    return (
        <TabHeaderComponent
            onClick={props.onClick}
            style={props.style}
            className={"tab-header " + props.className}>
            <label className="title">
                {props.title}
            </label>
        </TabHeaderComponent>
    )
};

class TabComponent extends Component {

    state = {
        selectedTab: this.props.tabs[0].title
    };

    handleTabChange = (tabTitle) => {
        this.setState({selectedTab: tabTitle});
    };

    render() {
        return (
            <div>
                <FlexBox>
                    {
                        this.props.tabs.map((tab) => <TabHeader
                            onClick={() => this.handleTabChange(tab.title)}
                            className={this.state.selectedTab == tab.title ? "active" : ""}
                            key={tab.title}
                            title={tab.title}/>)
                    }
                </FlexBox>
                <FlexBox justifyContent="center" style={{padding: 25}}>
                    {
                        this.props.tabs.find((tab) => tab.title == this.state.selectedTab).component
                    }
                </FlexBox>
            </div>
        )
    }

}

export default TabComponent;