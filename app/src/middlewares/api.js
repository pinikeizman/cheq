import actions from '../actions/index';
import fetch from 'isomorphic-fetch';

export default store => next => action => {

    if (action.type == actions.API) {
        const {
            url = '/',
            body = undefined,
            method = "GET",
            accept = "application/json",
            contentType = 'application/json; charset=utf-8'
        } = action.payload;

        next(action);

        return fetch(url,
            {
                method,
                headers: {
                    'Accept': accept,
                    "Content-Type":contentType
                },
                body
            });
    }
    next(action);


};