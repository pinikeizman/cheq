import api from './api';
import logger from './logger';

const middlewares = [];

middlewares.push(api);

if(process.env.NODE_ENV == 'development'){
    middlewares.push(logger);
}

export default middlewares;