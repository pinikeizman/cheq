const vastXmlTamplate = require('./vastXmlTemplate.js');

//possibles positions, mySql is neglecting CHECK constraints
const possiblePositions = ["top_left","top_middle","top_right",
							"middle_left","middle_right","bottom_left",
							"bottom_middle","bottom_right"];
// url matching regex
const urlRegex =  /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/


/*
 *	Insert vast query getter
 */
const createInsertVastQuery = function(vast){
	const  query = "INSERT INTO `CHEQ`.`Vasts` " +
		"(`vast_url`,`position`,`hide_ui`)" +
		" VALUES (?,?,?);";
	return query;
}

/*
 *	Vast xml parser 
 * 	@param vast : vast object
 * 	@returns : string represent the xml object
 */
const createVastXml = (vast)=>{
	const replacementsTags = {
		vastURL:"__vastURL",
		position:"__position",
		hideUI:"__hideUI",
		vastId:"__vastId"
	}
	var vastInXml = vastXmlTamplate
					.replace(new RegExp(replacementsTags.vastURL,'g'),vast.vast_url)
					.replace(new RegExp(replacementsTags.position,'g'),vast.position)
					.replace(new RegExp(replacementsTags.hideUI,'g'),vast.hide_ui ? "true" : "false" )
					.replace(new RegExp(replacementsTags.vastId,'g'),vast.id);

	return vastInXml;
}

/*
 *	Select vast query getter
 */
const selectVast = ()=>{
	return "SELECT * FROM `CHEQ`.`Vasts` WHERE id = ?";
}
/*
 *	Validate vast function
 *	@param vast : vast object
 *	@returns : [string] array of errors | undefined
 */
const validateCreateVast = (vast)=>{
	const booleanValues = ["0","1","true","false"];
	const error = [];
	if (!vast.vastURL) {
		error.push("vastURL is required.");
	}else{
		if(!urlRegex.test(vast.vastURL)){
			error.push("vastURL must be a valid url.");
		}
	}
	if (vast.hideUI && !booleanValues.find((boolValue)=> boolValue == vast.hideUI)) {
		error.push("hideUI must be bool.");
	}
	if(vast.position && 
		!possiblePositions.find((position)=> position == vast.position)){
		error.push("vast position is invalid.");
	}
	return error.length == 0 ? undefined : error; 
}

module.exports = {
	insertVast: createInsertVastQuery,
	createVastXml,
	selectVast,
	validateCreateVast
}