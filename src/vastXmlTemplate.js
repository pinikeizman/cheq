module.exports = `<VAST version="2.0">
<Ad id="ComboGuard">
<InLine>
<AdSystem>2.0</AdSystem>
<Impression/>
<Creatives>
<Creative>
<Linear>
<Duration>00:00:19</Duration>
<MediaFiles>
<MediaFile type="application/x-shockwave-flash" apiFramework="VPAID" height="168" width="298" delivery="progressive">
<![CDATA[
http://localhost/ComboWrapper.swf?vast=__vastURL&position=__position&hideUI=__hideUI&videoId=__vastId
]]>
</MediaFile>
<MediaFile type="application/javascript" apiFramework="VPAID" height="168" width="298" delivery="progressive">
<![CDATA[
http://localhost/ComboWrapper.js?vast=__vastURL&position=__position&hideUI=__hideUI&videoId=__vastId
]]>
</MediaFile>
</MediaFiles>
</Linear>
</Creative>
</Creatives>
</InLine>
</Ad>
</VAST>`