const express = require('express');
const path = require("path")
const app = express();
const mysql = require('mysql');
const _ = require("lodash");
const helper = require("./helper.js");
const credentials = require('./credentials');

console.log(process.env.MY_SQL_USERNAME, process.env.MY_SQL_PASSWORD );


// mysql connection
const sqlCon = mysql.createConnection({
    host: 'localhost',
    user: process.env.MY_SQL_USERNAME || credentials.MY_SQL.USERNAME,
    password: process.env.MY_SQL_PASSWORD || credentials.MY_SQL.PASSWORD,
    database: 'CHEQ'
});
sqlCon.connect((err) => {
    if (err) throw err;

    console.log('Connected to mySql!');
});

// lest hide the header with the server vendor and version
app.disable('x-powered-by');

//use bodyparser for json body parsing
app.use(require('body-parser').json());
app.use(function (error, req, res, next) {
    //Catch json error
    res.status(400);
    res.type("application/json");
    res.send({error: "Error parsing json."});
});

//set port from env with default 3000
app.set('port', process.env.PORT | 3000);

//set `/public` as static content directory to serv our index.hml (landing point)
const publicDirPath = path.resolve(__dirname, "..") + '/public';
console.log("Server serv static content on dir: \n", publicDirPath);
app.use(express.static(publicDirPath));

//							*** API ***	

/*
 *	Get /fetch_vast?id=<vast_id> -> fetch vast with id of <vast_id>
 */
app.get('/fetch_vast', function (req, res) {
    const vastId = req.query.id;
    console.log("vastId\n", vastId);
    if (vastId) {
        const selectQuery = helper.selectVast(vastId);
        console.log("select query:\n", selectQuery);
        sqlCon.query(selectQuery, [vastId], function (err, rows) {
            if (err) {
                console.log(err);
                res.status(500);
                res.send("Error 500");
            } else {
                var body = undefined;
                if (rows.length == 0) {
                    res.status(404);
                    body = "<VAST version=\"2.0\"></VAST>";
                } else {
                    res.status(200);
                    body = helper.createVastXml(rows[0]);
                }
                res.type("application/xml");
                res.send(body);
            }
        });
    } else {
        res.status(400);
        res.type("application/json");
        res.send(Object.assign({error: "no vast URL in request object."}));
    }

});
// api post creat vast
app.post('/create_vast', function (req, res) {
    // console.log("inside /create_vast body:",req.body);
    const vast = req.body;
    console.log("vast\n", vast);
    const errors = helper.validateCreateVast(vast);
    if (!errors) {
        vast.hideUI = vast.hideUI == "true" || vast.hideUI == "1" ? "1" : "0";
        vast.position = !vast.position ? "bottom_right" : vast.position;
        const insetQuery = helper.insertVast();
        console.log("insetQuery", insetQuery);
        //execute query
        sqlCon.query(insetQuery, [
            encodeURIComponent(vast.vastURL),
            encodeURIComponent(vast.position),
            encodeURIComponent(vast.hideUI)
        ], function (err, rows) {
            if (err) {
                console.log(err);
                res.status(500);
                res.send("Error 500");
            }
            ;
            res.status(200);
            res.type("application/json");
            res.send({insertId: rows.insertId});
        });
    } else {
        res.status(400);
        res.type("application/json");
        res.send(Object.assign(errors));
    }
});

//404 redirects
app.use(function (req, res) {
    const path404 = publicDirPath + '/index.html';
    console.log(path404);
    res.type('application/json');
    res.status(404);
    res.send({error: 'Not Found'});
    console.log('404 Not found\n');
});

app.listen(app.get('port'), function () {
    console.log('Express started pree ctl+c to terminate')
});